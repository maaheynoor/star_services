import 'package:flutter/material.dart';
import 'package:star_services/utils/colours.dart';
import 'package:star_services/utils/routes.dart';

class SquareCard extends StatelessWidget {
  SquareCard({this.img, this.title, this.onTap, this.textStyle});
  final img, title, onTap, textStyle;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GestureDetector(
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              child: Container(
                padding: EdgeInsets.all(10),
                height: MediaQuery.of(context).size.width / 4,
                width: MediaQuery.of(context).size.width / 4,
                child: Image.asset(img, fit: BoxFit.contain),
              ),
            ),
            onTap: onTap),
        Text(
          title,
          style: textStyle,
        ),
        SizedBox(
          height: 10,
        )
      ],
    );
  }
}

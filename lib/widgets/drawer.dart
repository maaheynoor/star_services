import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:star_services/utils/colours.dart';
import 'package:star_services/utils/routes.dart';

class DrawerData extends StatefulWidget {
  const DrawerData({Key key}) : super(key: key);

  @override
  _DrawerDataState createState() => _DrawerDataState();
}

class _DrawerDataState extends State<DrawerData> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              Container(
                height: 90.0,
                child: DrawerHeader(
                    decoration: BoxDecoration(
                      color: Colours.red,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Asir David',
                          style: TextStyle(color: Colors.white, fontSize: 16),
                        ),
                        Text(
                          '+91 - 91111111111',
                          style: TextStyle(color: Colors.white, fontSize: 16),
                        )
                      ],
                    )),
              ),
              ListTile(
                leading: Icon(Icons.home),
                title: Text('Home'),
              ),
              Divider(
                height: 2,
                thickness: 2,
              ),
              ListTile(
                leading: Icon(FontAwesome.car),
                title: Text('Change Car'),
              ),
              Divider(
                height: 2,
                thickness: 2,
              ),
              ListTile(
                leading: Icon(Icons.feedback),
                title: Text('Feedback'),
              ),
              Divider(
                height: 2,
                thickness: 2,
              ),
              ListTile(
                leading: Icon(Icons.info_outline),
                title: Text('About Us'),
              ),
              Divider(
                height: 2,
                thickness: 2,
              ),
              ListTile(
                leading: Icon(Icons.chat),
                title: Text('FAQ'),
              ),
              Divider(
                height: 2,
                thickness: 2,
              ),
              ListTile(
                leading: Icon(Icons.settings_outlined),
                title: Text('Settings'),
              ),
              Divider(
                height: 2,
                thickness: 2,
              ),
              ListTile(
                leading: Icon(Icons.logout),
                title: Text('Logout'),
                onTap: () {
                  Navigator.pushNamedAndRemoveUntil(context, select_category,
                      (Route<dynamic> route) => false);
                },
              ),
              Divider(
                height: 2,
                thickness: 2,
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text('Version: v1.43.2', style: TextStyle(color: Colors.grey)),
        ),
      ],
    );
  }
}

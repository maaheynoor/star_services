import 'package:flutter/material.dart';
import 'package:star_services/utils/colours.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:star_services/utils/routes.dart';
import 'package:star_services/screens/select_garage.dart';

class ServiceCard extends StatelessWidget {
  ServiceCard({this.data});
  final data;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, call_screen, arguments: false);
      },
      child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  children: [
                    Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                      child: Container(
                        height: 50,
                        width: 50,
                        child: Image.asset(data['image'], fit: BoxFit.contain),
                      ),
                    ),
                    Text.rich(TextSpan(children: [
                      WidgetSpan(
                        child: Icon(FontAwesome.inr, size: 14),
                      ),
                      TextSpan(
                        text: data['price'].toString(),
                      ),
                    ]))
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      data['title'],
                      style:
                          TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    for (var i = 0; i < data['details'].length; i++)
                      Text(data['details'][i]),
                  ],
                ),
                TextButton(
                    child: Text(
                      'Add',
                      style: TextStyle(color: Colors.white),
                    ),
                    style: TextButton.styleFrom(
                        backgroundColor: Colours.red,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20))),
                    onPressed: () {
                      Navigator.pushNamed(context, select_garage);
                    }),
              ],
            ),
          )),
    );
  }
}

import 'package:flutter/material.dart';

class StarRating extends StatelessWidget {
  StarRating(this.rating);
  final rating;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        for (var j = 0; j < rating; j++)
          Icon(
            Icons.star,
            color: Colors.green,
            size: 16,
          ),
        for (var j = rating; j < 5; j++)
          Icon(
            Icons.star_outline,
            color: Colors.black,
            size: 16,
          ),
      ],
    );
  }
}

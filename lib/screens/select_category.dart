import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'package:star_services/utils/colours.dart';
import 'package:star_services/utils/routes.dart';

class CategoryPage extends StatefulWidget {
  const CategoryPage({Key key}) : super(key: key);

  @override
  _CategoryPageState createState() => _CategoryPageState();
}

class _CategoryPageState extends State<CategoryPage> {
  String selected = "User";
  Widget CategoryCard(title) {
    return GestureDetector(
      onTap: () {
        setState(() {
          selected = title;
        });
        if (title == "User")
          Navigator.pushNamed(context, register);
        else if (title == "Mechanic")
          Navigator.pushNamed(context, register_mechanic);
        else if (title == "Garage")
          Navigator.pushNamed(context, register_garage);
      },
      child: Card(
        margin: EdgeInsets.symmetric(horizontal: 50, vertical: 5),
        color: selected == title ? Colours.red : Colors.white,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: [
              CircleAvatar(
                radius: 25,
                backgroundImage: AssetImage(
                    'assets/images/' + title.toLowerCase() + '_icon.PNG'),
              ),
              SizedBox(
                width: 50,
              ),
              Text(
                title,
                style: TextStyle(
                  color: selected == title ? Colors.white : Colors.black,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Image.asset(
            "assets/images/ss-logo-dark.png",
            height: 50,
          ),
        ),
        elevation: 0,
        backgroundColor: Colors.white,
      ),
      body: SafeArea(
          child: Center(
        child: Column(
          // mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Stack(
              children: [
                Container(
                  width: double.infinity,
                  child: Image.asset(
                    'assets/images/pink-shape.PNG',
                    fit: BoxFit.fill,
                  ),
                ),
                Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 20),
                      child: Text(
                        'Select Your Category',
                        style: TextStyle(
                            color: Colours.red, fontWeight: FontWeight.bold),
                      ),
                    ),
                    CategoryCard('User'),
                    CategoryCard('Mechanic'),
                    CategoryCard('Garage')
                  ],
                )
              ],
            ),
            Text.rich(
              TextSpan(
                  style: TextStyle(fontWeight: FontWeight.bold),
                  children: <TextSpan>[
                    TextSpan(
                        text: "Already have an account? ",
                        style: TextStyle(color: Colors.black87)),
                    TextSpan(
                      text: "Login",
                      style: TextStyle(color: Colors.blue),
                      recognizer: TapGestureRecognizer()
                        ..onTap = () {
                          // Navigator.pushReplacementNamed(context, login);
                        },
                    ),
                  ]),
            ),
          ],
        ),
      )),
      resizeToAvoidBottomInset: false,
    );
  }
}

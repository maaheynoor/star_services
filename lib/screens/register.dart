import 'package:flutter/material.dart';
import 'package:star_services/utils/colours.dart';
import 'package:star_services/utils/routes.dart';
import 'package:intl_phone_number_input/intl_phone_number_input.dart';

class Register extends StatefulWidget {
  const Register({Key key}) : super(key: key);

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          appBar: AppBar(
            title: Center(
              child: Image.asset(
                "assets/images/ss-logo-white.png",
                height: 50,
              ),
            ),
            elevation: 0,
            actions: [
              IconButton(icon: Icon(Icons.help_outline), onPressed: () {})
            ],
            backgroundColor: Colours.red,
            shape: ContinuousRectangleBorder(
              borderRadius: const BorderRadius.only(
                bottomLeft: Radius.circular(20.0),
                bottomRight: Radius.circular(20.0),
              ),
            ),
          ),
          body: SingleChildScrollView(
              padding: EdgeInsets.all(20),
              child: Column(children: [
                Image.asset('assets/images/mob.PNG',
                    height: MediaQuery.of(context).size.height / 3),
                SizedBox(height: 10),
                Text(
                  'Enter your mobile number',
                  style: TextStyle(
                      color: Colours.red, fontWeight: FontWeight.bold),
                ),
                SizedBox(height: 10),
                InternationalPhoneNumberInput(
                  onInputChanged: (PhoneNumber number) {
                    print(number.phoneNumber);
                  },
                  onInputValidated: (bool value) {
                    print(value);
                  },
                  selectorConfig: SelectorConfig(
                    showFlags: false,
                    useEmoji: true,
                  ),
                  ignoreBlank: false,
                  autoValidateMode: AutovalidateMode.disabled,
                  selectorTextStyle: TextStyle(color: Colors.black),
                  formatInput: false,
                  keyboardType: TextInputType.numberWithOptions(
                      signed: true, decimal: true),
                  inputDecoration: InputDecoration(
                    filled: true,
                    fillColor: Colours.grey,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0),
                      borderSide: BorderSide.none,
                    ),
                  ),
                  onSaved: (PhoneNumber number) {
                    print('On Saved: $number');
                  },
                ),
                SizedBox(height: 10),
                Text(
                  "You'll receive a 4 digit code on this mobile number",
                  style: TextStyle(color: Colors.black38),
                ),
                SizedBox(height: 30),
                TextButton(
                  onPressed: () {
                    Navigator.pushReplacementNamed(context, otp);
                  },
                  child: Text(
                    'GET OTP',
                    style: TextStyle(color: Colors.white),
                  ),
                  style: TextButton.styleFrom(
                      backgroundColor: Colours.red,
                      minimumSize:
                          Size(MediaQuery.of(context).size.width * 0.9, 40),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20))),
                ),
              ]))),
    );
  }
}

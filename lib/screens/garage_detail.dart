import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:star_services/utils/colours.dart';
import 'package:star_services/widgets/starRating.dart';
import 'package:star_services/utils/routes.dart';

class GarageDetail extends StatefulWidget {
  const GarageDetail({Key key}) : super(key: key);

  @override
  _GarageDetailState createState() => _GarageDetailState();
}

class _GarageDetailState extends State<GarageDetail> {
  bool _confirmed = false;
  DateTime _selectedDate = DateTime.now();
  var _datecontroller = TextEditingController();
  TimeOfDay selectedTime = TimeOfDay(hour: 00, minute: 00);
  var _timecontroller = TextEditingController();
  Future _selectDate() async {
    DateTime picked = await showDatePicker(
        context: context,
        initialDate: _selectedDate,
        firstDate: DateTime.now(),
        lastDate: DateTime(2501),
        builder: (BuildContext context, Widget child) {
          return Theme(
            data: ThemeData.dark().copyWith(
              colorScheme: ColorScheme.light(
                  primary: Colours.red,
                  onPrimary: Colors.white,
                  surface: Colours.red,
                  onSurface: Colors.pink),
              dialogBackgroundColor: Colours.pink,
            ),
            child: child,
          );
        });
    if (picked != null)
      setState(() {
        _selectedDate = picked;
        _datecontroller.text = DateFormat('dd/MM/yyyy').format(picked);
      });
  }

  Future _selectTime() async {
    final TimeOfDay picked = await showTimePicker(
        context: context,
        initialTime: selectedTime,
        builder: (BuildContext context, Widget child) {
          return Theme(
            data: ThemeData.dark().copyWith(
              colorScheme: ColorScheme.light(
                  primary: Colours.red,
                  onPrimary: Colors.red,
                  surface: Colours.pink,
                  onSurface: Colors.pink),
              dialogBackgroundColor: Colors.white,
            ),
            child: child,
          );
        });
    if (picked != null)
      setState(() {
        selectedTime = picked;
        var am_pm = selectedTime.period.toString().toUpperCase();
        _timecontroller.text =
            selectedTime.hourOfPeriod.toString().padLeft(2, '0') +
                ":" +
                selectedTime.minute.toString().padLeft(2, '0') +
                " " +
                am_pm.substring(am_pm.length - 2);
        print(_timecontroller.text);
      });
  }

  Future<void> _showDialog() async {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                  child: Text(
                'Brand New Auto',
                style: TextStyle(fontSize: 14),
              )),
              SizedBox(width: 5),
              Text(
                'Basic Service',
                style: TextStyle(fontSize: 12),
              ),
            ],
          ),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text.rich(TextSpan(children: [
                  TextSpan(
                      text: 'Date',
                      style: TextStyle(fontWeight: FontWeight.bold)),
                  TextSpan(text: '- ${_datecontroller.text}'),
                ])),
                Text.rich(TextSpan(children: [
                  TextSpan(
                      text: 'Time',
                      style: TextStyle(fontWeight: FontWeight.bold)),
                  TextSpan(text: '- ${_timecontroller.text}'),
                ])),
                Text('Your Address',
                    style: TextStyle(fontWeight: FontWeight.bold)),
                Text(
                    '249 , S I H S Colony, Aerodrome Post,Coimbatore, Mumbai- 400043')
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
                setState(() {
                  _confirmed = true;
                });
              },
              child: Text(
                'CONFIRM BOOKING',
                style: TextStyle(color: Colors.white),
              ),
              style: TextButton.styleFrom(
                  backgroundColor: Colours.red,
                  minimumSize: Size(MediaQuery.of(context).size.width, 40),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20))),
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Periodic services'),
          elevation: 0,
          actions: [
            IconButton(icon: Icon(Icons.help_outline), onPressed: () {})
          ],
          backgroundColor: Colours.red,
          shape: ContinuousRectangleBorder(
            borderRadius: const BorderRadius.only(
              bottomLeft: Radius.circular(20.0),
              bottomRight: Radius.circular(20.0),
            ),
          ),
        ),
        body: _confirmed
            ? Center(
                child: Container(
                  margin: EdgeInsets.all(10),
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      border: Border.all(width: 1, color: Colours.grey),
                      borderRadius: BorderRadius.circular(5)),
                  height: MediaQuery.of(context).size.height / 4,
                  child: Column(
                    children: [
                      Stack(
                        alignment: Alignment.center,
                        children: [
                          Icon(
                            Icons.shield,
                            size: 70,
                            color: Colors.green,
                          ),
                          Icon(
                            Icons.check,
                            color: Colors.white,
                          )
                        ],
                      ),
                      Text(
                        'You have successfully booked your periodic service for your car',
                        style: TextStyle(fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                      ),
                      TextButton(
                        onPressed: () {
                          Navigator.pushNamedAndRemoveUntil(
                              context, dashb, (route) => false);
                        },
                        child: Text(
                          'BACK TO HOME',
                          style: TextStyle(color: Colors.white),
                        ),
                        style: TextButton.styleFrom(
                            backgroundColor: Colours.red,
                            padding: EdgeInsets.symmetric(horizontal: 40),
                            minimumSize: Size(
                                MediaQuery.of(context).size.width * 0.6, 40),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20))),
                      ),
                    ],
                  ),
                ),
              )
            : SingleChildScrollView(
                padding: const EdgeInsets.all(10),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding:
                            EdgeInsets.symmetric(vertical: 10, horizontal: 30),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              'Brand New Auto',
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.bold),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            StarRating(4),
                            SizedBox(
                              height: 8,
                            ),
                            Text(
                              '249 , S I H S Colony, Aerodrome Post,Coimbatore, Mumbai- 400043',
                              textAlign: TextAlign.center,
                            ),
                            Text(
                              'View in map',
                              style:
                                  TextStyle(fontSize: 16, color: Colors.blue),
                            ),
                            SizedBox(
                              height: 15,
                            ),
                            Text(
                              'Schedule your basic service',
                              style: TextStyle(fontSize: 18),
                            ),
                          ],
                        ),
                      ),
                      Row(
                        children: [
                          Expanded(
                            flex: 1,
                            child: Column(
                              children: [
                                Text('Date'),
                                DateTimeInput(
                                  onTap: _selectDate,
                                  controller: _datecontroller,
                                )
                              ],
                            ),
                          ),
                          Expanded(
                            flex: 1,
                            child: Column(
                              children: [
                                Text('Time'),
                                DateTimeInput(
                                    onTap: _selectTime,
                                    controller: _timecontroller,
                                    date: false)
                              ],
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        'Your Address',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: Text(
                                '249 , S I H S Colony, Aerodrome Post,Coimbatore, Mumbai- 400043'),
                          ),
                          OutlinedButton(
                            onPressed: () {},
                            child: Text('Change'),
                            style: OutlinedButton.styleFrom(
                                primary: Colors.black,
                                side: BorderSide(color: Colors.black),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(30.0),
                                )),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Center(
                        child: TextButton(
                          onPressed: _showDialog,
                          child: Text(
                            'BOOK NOW',
                            style: TextStyle(color: Colors.white),
                          ),
                          style: TextButton.styleFrom(
                              backgroundColor: Colours.red,
                              padding: EdgeInsets.symmetric(horizontal: 40),
                              minimumSize: Size(
                                  MediaQuery.of(context).size.width * 0.6, 40),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20))),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Customer Reviews',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          StarRating(4),
                        ],
                      ),
                      SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Row(
                          children: [
                            ReviewCard(),
                            ReviewCard(),
                            ReviewCard(),
                          ],
                        ),
                      ),
                    ])),
      ),
    );
  }
}

class DateTimeInput extends StatelessWidget {
  DateTimeInput({this.onTap, this.controller, this.date = true});
  final onTap, controller, date;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: GestureDetector(
        onTap: onTap,
        child: AbsorbPointer(
          child: TextField(
            controller: controller,
            decoration: InputDecoration(
                filled: true,
                fillColor: Colours.grey,
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5.0),
                  borderSide: BorderSide.none,
                ),
                hintText: date ? 'DD/MM/YYY' : 'hh:mm',
                suffixIcon: date
                    ? IconButton(
                        icon: Icon(Icons.calendar_today_outlined),
                        color: Colors.black54,
                        onPressed: () {},
                      )
                    : null),
          ),
        ),
      ),
    );
  }
}

class ReviewCard extends StatelessWidget {
  const ReviewCard({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(10),
      width: MediaQuery.of(context).size.width * 0.8,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10), color: Colours.pink),
      child: Column(
        children: [
          ClipPath(
            clipper: CustomClipPath(),
            child: Container(
                padding: EdgeInsets.all(10),
                height: 70,
                decoration: BoxDecoration(
                    borderRadius:
                        BorderRadius.vertical(top: Radius.circular(10)),
                    color: Colours.red),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    ClipOval(
                      child: Image.asset(
                        "assets/images/user_icon.PNG",
                        height: 50,
                        width: 50,
                        fit: BoxFit.cover,
                      ),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Text(
                      'John Wick',
                      style: TextStyle(color: Colors.white, fontSize: 16),
                    ),
                  ],
                )),
          ),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Text(
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod temporincididunt ut labore et dolore magnaaliqua. Bibendum est ultricies integer quis."'),
          ),
        ],
      ),
    );
  }
}

class CustomClipPath extends CustomClipper<Path> {
  var radius = 10.0;
  @override
  Path getClip(Size size) {
    Path path = Path();
    path.lineTo(0, size.height);
    path.quadraticBezierTo(
        size.width / 4, size.height - 40, size.width / 2, size.height - 20);
    path.quadraticBezierTo(
        3 / 4 * size.width, size.height, size.width, size.height);
    path.lineTo(size.width, 0);

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}

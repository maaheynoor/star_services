import 'package:flutter/material.dart';
import 'package:star_services/utils/colours.dart';
import 'package:star_services/widgets/serviceCard.dart';
import 'package:star_services/screens/dashboard.dart';

class ServicesTabsScreen extends StatefulWidget {
  @override
  _ServicesTabsScreenState createState() => _ServicesTabsScreenState();
}

class _ServicesTabsScreenState extends State<ServicesTabsScreen> {
  List services = [
    {
      "title": "Basic Service",
      "details": [
        "Every 5000 Kms/3 Months",
        "Takes 3 Hours",
        "1 Month Warranty",
        "Includes 9 Services"
      ],
      "image": "assets/images/service1.PNG",
      "price": 3450
    },
    {
      "title": "Standard Service",
      "details": [
        "Every 5000 Kms/3 Months",
        "Takes 3 Hours",
        "1 Month Warranty",
        "Includes 9 Services"
      ],
      "image": "assets/images/service1.PNG",
      "price": 4450
    },
    {
      "title": "Comprehensive Service",
      "details": [
        "Every 5000 Kms/3 Months",
        "Takes 3 Hours",
        "1 Month Warranty",
        "Includes 9 Services"
      ],
      "image": "assets/images/service1.PNG",
      "price": 3450
    }
  ];
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Periodic services'),
          elevation: 0,
          actions: [
            IconButton(icon: Icon(Icons.help_outline), onPressed: () {})
          ],
          backgroundColor: Colours.red,
          shape: ContinuousRectangleBorder(
            borderRadius: const BorderRadius.only(
              bottomLeft: Radius.circular(20.0),
              bottomRight: Radius.circular(20.0),
            ),
          ),
        ),
        body: DefaultTabController(
          length: 2,
          child: Scaffold(
            appBar: AppBar(
              automaticallyImplyLeading: false,
              backgroundColor: Colors.white,
              flexibleSpace: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  TabBar(
                    indicatorColor: Colours.red,
                    labelColor: Colours.red,
                    unselectedLabelColor: Colours.red,
                    labelStyle: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                    tabs: <Widget>[
                      Tab(
                        text: 'Scheduled Packages',
                      ),
                      Tab(
                        text: 'Brake Maintenance',
                      ),
                    ],
                  ),
                ],
              ),
            ),
            body: TabBarView(
              children: [
                CustomTab(
                  title: 'Scheduled Packages',
                  services: services,
                ),
                CustomTab(title: 'Brake Maintenance', services: services),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class CustomTab extends StatelessWidget {
  CustomTab({this.title, this.services});
  final title, services;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: EdgeInsets.all(10),
      child: Column(children: [
        TextField(
          decoration: InputDecoration(
              contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              filled: true,
              fillColor: Colours.grey,
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5.0),
                borderSide: BorderSide.none,
              ),
              hintText: 'Search here',
              hintStyle: TextStyle(
                color: Colors.black38,
              ),
              prefixIcon: Icon(Icons.search)),
        ),
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: Text(
            title,
            style: TextStyle(color: Colours.red, fontWeight: FontWeight.bold),
          ),
        ),
        for (var i = 0; i < services.length; i++) ServiceCard(data: services[i])
      ]),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:star_services/utils/colours.dart';
import 'package:star_services/utils/routes.dart';
import 'package:star_services/widgets/squareCard.dart';

class CarBrand extends StatefulWidget {
  const CarBrand({Key key}) : super(key: key);

  @override
  _CarBrandState createState() => _CarBrandState();
}

class _CarBrandState extends State<CarBrand> {
  List<String> brands = [
    "KIA",
    "Skoda",
    "Honda",
    "TATA",
    "Mercedez",
    "Ford",
    "Mahindra",
    "Volkswagen",
    "Volvo",
    "Porsche",
    "Mini Cooper",
    "Audi"
  ];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            appBar: AppBar(
              title: Center(
                child: Image.asset(
                  "assets/images/ss-logo-white.png",
                  height: 50,
                ),
              ),
              elevation: 0,
              actions: [
                IconButton(icon: Icon(Icons.help_outline), onPressed: () {})
              ],
              backgroundColor: Colours.red,
              shape: ContinuousRectangleBorder(
                borderRadius: const BorderRadius.only(
                  bottomLeft: Radius.circular(20.0),
                  bottomRight: Radius.circular(20.0),
                ),
              ),
            ),
            body: SingleChildScrollView(
              padding: EdgeInsets.all(10),
              child: Column(children: [
                TextField(
                  decoration: InputDecoration(
                      contentPadding:
                          EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                      filled: true,
                      fillColor: Colours.grey,
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5.0),
                        borderSide: BorderSide.none,
                      ),
                      hintText: 'Search by modal or brand name',
                      hintStyle: TextStyle(
                        color: Colors.black38,
                      ),
                      prefixIcon: Icon(Icons.search)),
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Text(
                    'Select Your Car Brand',
                    style: TextStyle(
                        color: Colours.red, fontWeight: FontWeight.bold),
                  ),
                ),
                for (var i = 0; i < brands.length / 3; i++)
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      for (var j = 0; j < 3; j++)
                        SquareCard(
                            img: 'assets/images/car_brands/' +
                                brands[i * 3 + j].toLowerCase() +
                                '.PNG',
                            title: brands[i * 3 + j],
                            onTap: () {
                              Navigator.pushNamed(context, car_model,
                                  arguments: brands[i * 3 + j]);
                            },
                            textStyle: TextStyle(color: Colors.black))
                    ],
                  )
              ]),
            )));
  }
}

import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'package:star_services/utils/colours.dart';
import 'package:star_services/utils/routes.dart';

class OTP extends StatefulWidget {
  var userData;
  OTP({this.userData});

  @override
  _OTPState createState() => _OTPState();
}

class _OTPState extends State<OTP> {
  int otp_screen = 0; //0- enter otp, 1- verified, 2- failed
  List otp_entered = ['-', '-', '-', '-'];
  bool isDigit(String s) =>
      "0".compareTo(s[0]) <= 0 && "9".compareTo(s[0]) >= 0;
  String _error = "";
  String verificationId;
  // CollectionReference users = FirebaseFirestore.instance.collection('users');

  void sendOTP(String phone) async {
    // FirebaseAuth _auth = FirebaseAuth.instance;
    // _auth.verifyPhoneNumber(
    //     phoneNumber: phone,
    //     verificationCompleted: (phoneAuthCredential) async {
    //       print("Verification Completed");
    //     },
    //     verificationFailed: (verificationFailed) async {
    //       print(verificationFailed.message);
    //     },
    //     codeSent: (verificationId, resendingToken) async {
    //       setState(() {
    //         this.verificationId = verificationId;
    //       });
    //     },
    //     codeAutoRetrievalTimeout: (verificationId) async {});
  }

  bool otpVerify(String otp) {
    // PhoneAuthCredential phoneAuthCredential = PhoneAuthProvider.credential(
    //     verificationId: verificationId, smsCode: otp);
    // // signInWithPhoneAuthCredential(phoneAuthCredential);
    // return phoneAuthCredential != null;
    if (otp == "1111") {
      return true;
    } else {
      return false;
    }
  }

  @override
  initState() {
    super.initState();
    // sendOTP(widget.userData['email_or_mob']);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Center(
            child: Image.asset(
              "assets/images/ss-logo-white.png",
              height: 50,
            ),
          ),
          elevation: 0,
          actions: [
            IconButton(icon: Icon(Icons.help_outline), onPressed: () {})
          ],
          backgroundColor: Colours.red,
          shape: ContinuousRectangleBorder(
            borderRadius: const BorderRadius.only(
              bottomLeft: Radius.circular(20.0),
              bottomRight: Radius.circular(20.0),
            ),
          ),
        ),
        body: SingleChildScrollView(
          padding: EdgeInsets.symmetric(vertical: 20),
          child: otp_screen == 0
              ? Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Center(
                      child: Image(
                          height: MediaQuery.of(context).size.height / 3,
                          image: AssetImage('assets/images/mob.PNG')),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      padding:
                          EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                      alignment: Alignment.center,
                      child: Text(
                        'Enter the 4 digit code sent to your mobile number',
                        style: TextStyle(
                            color: Colours.red, fontWeight: FontWeight.bold),
                      ),
                    ),
                    SizedBox(
                      height: 30.0,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        OTPInput(autofocus: true, index: 0),
                        OTPInput(index: 1),
                        OTPInput(index: 2),
                        OTPInput(index: 3),
                      ],
                    ),
                    _error != ""
                        ? Text(
                            _error,
                            style: TextStyle(
                              color: Colours.red,
                            ),
                          )
                        : Container(),
                    SizedBox(
                      height: 30.0,
                    ),
                    TextButton(
                        child: Text(
                          'VERIFY',
                          style: TextStyle(color: Colors.white),
                        ),
                        style: TextButton.styleFrom(
                            backgroundColor: Colours.red,
                            minimumSize: Size(
                                MediaQuery.of(context).size.width * 0.9, 40),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20))),
                        onPressed: () {
                          print(otp_entered);
                          if (otp_entered.every((e) => isDigit(e))) {
                            if (otpVerify(otp_entered.join())) {
                              setState(() {
                                otp_screen = 1;
                              });
                              // register();
                            } else
                              setState(() {
                                _error = "Incorrect OTP";
                              });
                          } else {
                            setState(() {
                              _error = "Please enter 4 digits OTP";
                            });
                          }
                        }),
                    SizedBox(
                      height: 10,
                    ),
                    Center(
                      child: Text.rich(
                        TextSpan(
                            style: TextStyle(fontWeight: FontWeight.bold),
                            children: <TextSpan>[
                              TextSpan(
                                  text: "Not Received? ",
                                  style: TextStyle(color: Colors.black87)),
                              TextSpan(
                                text: "Resend",
                                style: TextStyle(color: Colors.blue),
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () {
                                    // sendOTP(widget.userData['email_or_mob']);
                                  },
                              ),
                            ]),
                      ),
                    ),
                  ],
                )
              : OTPVerified(context),
        ));
  }

  Widget OTPInput({autofocus, index}) {
    return Container(
      width: 50.0,
      height: 50.0,
      decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment(
                0.8, 0.0), // 10% of the width, so there are ten blinds.
            colors: <Color>[Colours.grey, Colors.grey[100]], // red to yellow
          ),
          borderRadius: BorderRadius.circular(25)),
      margin: EdgeInsets.all(5.0),
      child: TextField(
        textAlign: TextAlign.center,
        keyboardType: TextInputType.number,
        maxLength: 1,
        onChanged: (value) {
          setState(() {
            otp_entered[index] = value;
          });
          if (value.length == 1) {
            FocusScope.of(context).nextFocus();
          }
        },
        decoration: InputDecoration(
          counterText: '',
          // filled: true,
          // fillColor: Colors.black12,
          border: OutlineInputBorder(
            // borderRadius: BorderRadius.circular(5.0),
            borderSide: BorderSide.none,
          ),
          hintText: '1',
          hintStyle: TextStyle(
            color: Colors.black38,
          ),
        ),
      ),
    );
  }

  Widget OTPVerified(context) {
    var img, text, btnText;
    img = 'assets/images/otp.PNG';
    text = 'You have successfully verified your number';
    btnText = 'CONTINUE';

    return Center(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image(
            height: MediaQuery.of(context).size.height / 2,
            image: AssetImage(img),
          ),
          SizedBox(
            height: 20,
          ),
          Text(
            text,
            style: TextStyle(color: Colours.red, fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 30,
          ),
          TextButton(
              child: Text(
                btnText,
                style: TextStyle(color: Colors.white),
              ),
              style: TextButton.styleFrom(
                  backgroundColor: Colours.red,
                  minimumSize:
                      Size(MediaQuery.of(context).size.width * 0.9, 40),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20))),
              onPressed: () async {
                if (otp_screen == 1) {
                  Navigator.pop(context);
                  Navigator.pushReplacementNamed(context, car_brand);
                }
              }),
        ],
      ),
    );
  }
}

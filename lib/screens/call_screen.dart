import 'package:flutter/material.dart';
import 'package:star_services/utils/colours.dart';
import 'package:star_services/utils/routes.dart';
import 'package:url_launcher/url_launcher.dart';

class CallScreen extends StatefulWidget {
  CallScreen({this.mechanic = false});
  final mechanic;

  @override
  _CallScreenState createState() => _CallScreenState();
}

class _CallScreenState extends State<CallScreen> {
  var _chosenValue = "Battery Issue";
  final Uri phoneUrl = Uri(
    scheme: 'tel',
    path: '+11111111111',
  );
  _makingPhoneCall() async {
    const url = 'tel://9876543210';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  Widget carDetail() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Your Car',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        SizedBox(height: 10),
        Container(
          decoration: BoxDecoration(
              border: Border.all(color: Colours.grey),
              borderRadius: BorderRadius.all(Radius.circular(30))),
          padding: EdgeInsets.all(15),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('Honda WRV Petrol'),
              Text(
                'Edit',
                style: TextStyle(color: Colours.red),
              )
            ],
          ),
        ),
        SizedBox(height: 20),
        Text(
          'Emergency Service',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        SizedBox(height: 10),
        Container(
          decoration: BoxDecoration(
              border: Border.all(color: Colours.grey),
              borderRadius: BorderRadius.all(Radius.circular(30))),
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: DropdownButtonHideUnderline(
            child: DropdownButton<String>(
              isExpanded: true,
              value: _chosenValue,
              items: <String>['Battery Issue', 'Diesel', 'CNG']
                  .map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(value),
                );
              }).toList(),
              onChanged: (String value) {
                setState(() {
                  _chosenValue = value;
                });
              },
            ),
          ),
        ),
        SizedBox(height: 30),
        Row(
          children: [
            Expanded(
              flex: 1,
              child: OutlinedButton(
                onPressed: () {},
                child: Text('CANCEL'),
                style: OutlinedButton.styleFrom(
                    primary: Colors.black,
                    side: BorderSide(color: Colors.black),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0),
                    )),
              ),
            ),
            SizedBox(
              width: 20,
            ),
            Expanded(
              flex: 1,
              child: TextButton(
                onPressed: () {
                  _makingPhoneCall();
                  Navigator.pushNamed(context, call_screen, arguments: true);
                },
                child: Text(
                  'CALL',
                  style: TextStyle(color: Colors.white),
                ),
                style: TextButton.styleFrom(
                    backgroundColor: Colours.red,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30))),
              ),
            ),
          ],
        )
      ],
    );
  }

  Widget mechanicDetail() {
    return Column(
      children: [
        Card(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(10.0),
                      child: Image.asset(
                        'assets/images/mechanic.PNG',
                        height: 80.0,
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      'ID0123',
                      style:
                          TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                    )
                  ],
                ),
                SizedBox(
                  width: 20,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Mechanic Name',
                      style:
                          TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                    ),
                    Text(
                      'John Wick',
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      'Mechanic Number',
                      style:
                          TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                    ),
                    Text(
                      '9123456789',
                    )
                  ],
                )
              ],
            ),
          ),
        ),
        Text(
          'Mechanic will reach in 10 mins',
        ),
        SizedBox(height: 10),
        SizedBox(height: 30),
        Row(
          children: [
            Expanded(
              flex: 1,
              child: TextButton(
                onPressed: () {
                  _makingPhoneCall();
                },
                child: Text(
                  'CALL AGAIN',
                  style: TextStyle(color: Colors.white),
                ),
                style: TextButton.styleFrom(
                    backgroundColor: Colours.red,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30))),
              ),
            ),
            SizedBox(
              width: 30,
            ),
            Expanded(
              flex: 1,
              child: TextButton(
                onPressed: () {
                  _makingPhoneCall();
                },
                child: Text(
                  'VIEW ON MAP',
                  style: TextStyle(color: Colors.white),
                ),
                style: TextButton.styleFrom(
                    backgroundColor: Colours.red,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30))),
              ),
            ),
          ],
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Stack(
                children: [
                  Image.asset(
                    'assets/images/google_map.jpg',
                    width: double.infinity,
                  ),
                  Center(
                    child: Card(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Current Location',
                              style: TextStyle(fontSize: 10),
                            ),
                            Text(
                              'degrytgerg rgetr3q',
                              style: TextStyle(
                                  fontSize: 14, fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                      ),
                    ),
                  )
                ],
              ),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: widget.mechanic ? mechanicDetail() : carDetail(),
              )
            ],
          ),
        ),
      ),
    );
  }
}

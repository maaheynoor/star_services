import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:star_services/utils/colours.dart';
import 'package:star_services/utils/routes.dart';
import 'package:flutter/gestures.dart';

class OnBoarding extends StatefulWidget {
  const OnBoarding({Key key}) : super(key: key);

  @override
  _OnBoardingState createState() => _OnBoardingState();
}

class _OnBoardingState extends State<OnBoarding> {
  var data = [
    {
      "title": "Car Service",
      "detail":
          "Lorem ipsum dolor sit amet, consecteturadipiscing elit, sed do eiusmod temporincididunt ut labore et dolore magna aliqua.Bibendum est ultricies integer quis",
      "image": "assets/images/onboard1.PNG"
    },
    {
      "title": "Tire Service",
      "detail":
          "Lorem ipsum dolor sit amet, consecteturadipiscing elit, sed do eiusmod temporincididunt ut labore et dolore magna aliqua.Bibendum est ultricies integer quis",
      "image": "assets/images/onboard2.PNG"
    },
    {
      "title": "Car Mechanics",
      "detail":
          "Lorem ipsum dolor sit amet, consecteturadipiscing elit, sed do eiusmod temporincididunt ut labore et dolore magna aliqua.Bibendum est ultricies integer quis",
      "image": "assets/images/onboard3.PNG"
    }
  ];
  final PageController _controller = PageController(
    initialPage: 0,
    keepPage: false,
  );

  void onNavigate() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool("isonboarding", false);
    Navigator.pushNamedAndRemoveUntil(
        context, select_category, (route) => false);
  }

  Widget _onBoarding(int index) {
    return Center(
      child: Column(
        // mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          SizedBox(
            height: 10,
          ),
          Stack(
            children: [
              Container(
                width: double.infinity,
                height: MediaQuery.of(context).size.height * 0.6,
                child: Image.asset(
                  'assets/images/pink-shape.PNG',
                  fit: BoxFit.fill,
                ),
              ),
              Column(
                children: [
                  SizedBox(
                    height: 50,
                  ),
                  Image.asset(
                    data[index]['image'],
                    width: MediaQuery.of(context).size.width * 0.7,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    data[index]['title'],
                    style: TextStyle(
                        color: Colours.red,
                        fontWeight: FontWeight.bold,
                        fontSize: 18),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text(data[index]['detail']),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      for (var i = 0; i < 3; i++)
                        Padding(
                          padding: const EdgeInsets.all(2.0),
                          child: Icon(
                            Icons.brightness_1,
                            size: 10,
                            color: i == index ? Colours.red : Colors.white,
                          ),
                        ),
                    ],
                  ),
                ],
              )
            ],
          ),
          TextButton(
            onPressed: () {
              onNavigate();
            },
            child: Text(
              'REGISTER YOUR CAR',
              style: TextStyle(color: Colors.white),
            ),
            style: TextButton.styleFrom(
                backgroundColor: Colours.red,
                minimumSize: Size(MediaQuery.of(context).size.width * 0.9, 40),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20))),
          ),
          Text.rich(
            TextSpan(
                style: TextStyle(fontWeight: FontWeight.bold),
                children: <TextSpan>[
                  TextSpan(
                      text: "Already have an account? ",
                      style: TextStyle(color: Colors.black87)),
                  TextSpan(
                    text: "Login",
                    style: TextStyle(color: Colors.blue),
                    recognizer: TapGestureRecognizer()
                      ..onTap = () {
                        // Navigator.pushReplacementNamed(context, login);
                      },
                  ),
                ]),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Image.asset(
            "assets/images/ss-logo-dark.png",
            height: 50,
          ),
        ),
        actions: [
          TextButton(
              child: Text(
                'Skip',
                style: TextStyle(
                  color: Colors.black,
                  decoration: TextDecoration.underline,
                ),
              ),
              onPressed: () {
                onNavigate();
              }),
        ],
        elevation: 0,
        backgroundColor: Colors.white,
      ),
      body: SafeArea(
        child: Container(
          width: double.infinity,
          height: double.infinity, //MediaQuery.of(context).size.height,
          child: PageView(
            physics: AlwaysScrollableScrollPhysics(),
            reverse: false,
            scrollDirection: Axis.horizontal,
            controller: _controller,
            pageSnapping: true,
            children: <Widget>[_onBoarding(0), _onBoarding(1), _onBoarding(2)],
          ),
        ),
      ),
      resizeToAvoidBottomInset: false,
    );
  }
}

class CurvePainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Paint paint_0 = new Paint()
      ..color = Color.fromARGB(255, 255, 234, 234)
      ..style = PaintingStyle.fill
      ..strokeWidth = 1;

    Path path_0 = Path();
    path_0.moveTo(size.width * 0.1343750, size.height * 0.1195000);
    path_0.quadraticBezierTo(size.width * 0.7562500, size.height * -0.1291200,
        size.width * 0.9734375, size.height * 0.1980000);
    path_0.cubicTo(
        size.width * 0.9108625,
        size.height * 0.7538800,
        size.width * 0.7482000,
        size.height * 0.7163800,
        size.width * 0.6934375,
        size.height * 0.9900000);
    path_0.cubicTo(
        size.width * 0.5727375,
        size.height * 0.9945000,
        size.width * 0.2528125,
        size.height * 0.8203800,
        size.width * 0.1528125,
        size.height * 0.7405000);
    path_0.quadraticBezierTo(size.width * 0.0708625, size.height * 0.5846200,
        size.width * 0.1343750, size.height * 0.1195000);
    path_0.close();

    canvas.drawPath(path_0, paint_0);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}

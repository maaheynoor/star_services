import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:star_services/utils/colours.dart';
import 'package:url_launcher/url_launcher.dart';

class AcceptedCustomer extends StatefulWidget {
  const AcceptedCustomer({Key key}) : super(key: key);

  @override
  _AcceptedCustomerState createState() => _AcceptedCustomerState();
}

class _AcceptedCustomerState extends State<AcceptedCustomer> {
  bool _reached = false;
  _makingPhoneCall() async {
    const url = 'tel:9876543210';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Center(
            child: Image.asset(
              "assets/images/ss-logo-white.png",
              height: 50,
            ),
          ),
          elevation: 0,
          actions: [
            IconButton(icon: Icon(Icons.help_outline), onPressed: () {})
          ],
          backgroundColor: Colours.red,
          shape: ContinuousRectangleBorder(
            borderRadius: const BorderRadius.only(
              bottomLeft: Radius.circular(20.0),
              bottomRight: Radius.circular(20.0),
            ),
          ),
        ),
        body: _reached
            ? Reached()
            : SingleChildScrollView(
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text('ETA - 10 Mins'),
                    ),
                    Container(
                      width: double.infinity,
                      child: Image.asset(
                        'assets/images/google_map.jpg',
                      ),
                    ),
                    SizedBox(height: 20),
                    DetailButton(
                        detail:
                            'Click on reached location after you reach the location of the customer',
                        text: 'REACHED LOCATION',
                        icon: FontAwesome.map_marker,
                        onTap: () {
                          setState(() {
                            _reached = true;
                          });
                        }),
                    SizedBox(height: 20),
                    DetailButton(
                      detail: "If you didn't find the location of the customer",
                      text: 'CALL CUSTOMER',
                      icon: Icons.call,
                      onTap: () {
                        _makingPhoneCall();
                      },
                    ),
                  ],
                ),
              ),
      ),
    );
  }
}

class DetailButton extends StatelessWidget {
  DetailButton({this.detail, this.text, this.icon, this.onTap});
  final detail, text, icon, onTap;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Column(
        children: [
          Text(
            detail,
            textAlign: TextAlign.center,
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          TextButton(
            onPressed: onTap,
            child: RichText(
              text: TextSpan(
                children: [
                  icon != null
                      ? WidgetSpan(
                          child: Icon(
                            icon,
                            size: 18,
                            color: Colors.white,
                          ),
                        )
                      : TextSpan(),
                  TextSpan(
                    text: " $text",
                  ),
                ],
              ),
            ),
            style: TextButton.styleFrom(
                minimumSize: Size(MediaQuery.of(context).size.width * 0.9, 40),
                backgroundColor: Colours.red,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30))),
          ),
        ],
      ),
    );
  }
}

class Reached extends StatelessWidget {
  const Reached({Key key}) : super(key: key);
  Future<void> _showDialog(context) async {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: Text('Sending live tracking location to the customer'),
          actions: <Widget>[
            SizedBox(
              width: MediaQuery.of(context).size.width,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      'OK',
                      style: TextStyle(color: Colors.white),
                    ),
                    style: TextButton.styleFrom(
                        backgroundColor: Colours.red,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20))),
                  ),
                ],
              ),
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        child: Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'ID 1525437',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
              ),
              Icon(Icons.notifications_none)
            ],
          ),
        ),
        Divider(height: 2, thickness: 2),
        Text(
          'Reach location at 2:47 PM',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        SizedBox(height: 20),
        Card(
          margin: EdgeInsets.symmetric(horizontal: 10),
          child: Padding(
            padding: EdgeInsets.all(15),
            child: Column(
              children: [
                DetailButton(
                  detail: 'After Solving the issue',
                  text: 'ISSUE SOLVED',
                ),
                SizedBox(height: 20),
                DetailButton(
                    detail:
                        "If issue was not solved & requires moving to garage",
                    text: 'MOVING TO GARAGE',
                    onTap: () {
                      _showDialog(context);
                    }),
              ],
            ),
          ),
        )
      ],
    ));
  }
}

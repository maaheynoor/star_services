import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:star_services/utils/colours.dart';

class MechanicIncome extends StatelessWidget {
  const MechanicIncome({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(
            children: [
              Image.asset(
                'assets/images/google_map.jpg',
                width: double.infinity,
              ),
              Center(child: Container())
            ],
          ),
          IncomeCard(),
          IncomeCard(),
        ],
      ),
    );
  }
}

class IncomeCard extends StatelessWidget {
  IncomeCard({this.income, this.service, this.today = false});
  final income, service, today;

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.all(10),
      color: Colours.pink,
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Today's Income"),
                Container(
                  width: 80,
                  alignment: Alignment.center,
                  padding: EdgeInsets.all(5),
                  decoration: BoxDecoration(
                      color: Colors.pink[100],
                      borderRadius: BorderRadius.circular(20)),
                  child: Text.rich(TextSpan(children: [
                    WidgetSpan(
                      child: Icon(FontAwesome.inr, size: 14),
                    ),
                    TextSpan(
                      text: '3450',
                    ),
                  ])),
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
              Text("Total Services Today"),
              Container(
                  width: 80,
                  alignment: Alignment.center,
                  padding: EdgeInsets.all(5),
                  decoration: BoxDecoration(
                      color: Colors.pink[100],
                      borderRadius: BorderRadius.circular(20)),
                  child: Text('13')),
            ])
          ],
        ),
      ),
    );
  }
}

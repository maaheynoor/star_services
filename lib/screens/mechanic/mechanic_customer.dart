import 'package:flutter/material.dart';
import 'package:star_services/utils/colours.dart';
import 'package:star_services/utils/routes.dart';
import 'package:url_launcher/url_launcher.dart';

class CustomerDetail extends StatefulWidget {
  const CustomerDetail({Key key}) : super(key: key);

  @override
  _CustomerDetailState createState() => _CustomerDetailState();
}

class _CustomerDetailState extends State<CustomerDetail> {
  _makingPhoneCall() async {
    const url = 'tel:9876543210';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Center(
            child: Column(
              children: [
                Text('ETA - 10 Mins'),
                Container(
                  height: 200,
                  width: 200,
                  child: Image.asset(
                    'assets/images/google_map.jpg',
                    fit: BoxFit.contain,
                  ),
                ),
                Text(
                  'New Customer',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ],
            ),
          ),
          SizedBox(height: 20),
          Text(
            'Car Model',
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          SizedBox(height: 10),
          Container(
            width: double.infinity,
            decoration: BoxDecoration(
                border: Border.all(color: Colours.grey),
                borderRadius: BorderRadius.all(Radius.circular(30))),
            padding: EdgeInsets.all(15),
            child: Text('Honda WRV Petrol'),
          ),
          SizedBox(height: 20),
          Text(
            'Problem',
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          SizedBox(height: 10),
          Container(
            width: double.infinity,
            decoration: BoxDecoration(
                border: Border.all(color: Colours.grey),
                borderRadius: BorderRadius.all(Radius.circular(30))),
            padding: EdgeInsets.all(15),
            child: Text('Battery Issue'),
          ),
          SizedBox(height: 20),
          Row(
            children: [
              Expanded(
                flex: 1,
                child: Expanded(
                  flex: 1,
                  child: TextButton(
                    onPressed: () {},
                    child: Text(
                      'DENY',
                      style: TextStyle(color: Colors.white),
                    ),
                    style: TextButton.styleFrom(
                        backgroundColor: Colours.red,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30))),
                  ),
                ),
              ),
              SizedBox(
                width: 20,
              ),
              Expanded(
                flex: 1,
                child: TextButton(
                  onPressed: () {
                    Navigator.pushNamed(context, accept_customer);
                  },
                  child: Text(
                    'ACCEPT',
                    style: TextStyle(color: Colors.white),
                  ),
                  style: TextButton.styleFrom(
                      backgroundColor: Colors.green,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30))),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}

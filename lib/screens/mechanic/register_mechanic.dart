import 'package:flutter/material.dart';
import 'package:star_services/utils/colours.dart';
import 'package:intl_phone_number_input/intl_phone_number_input.dart';
import 'package:star_services/utils/routes.dart';
import 'package:star_services/utils/inputstyle.dart';

class RegisterMechanic extends StatefulWidget {
  const RegisterMechanic({Key key}) : super(key: key);

  @override
  _RegisterMechanicState createState() => _RegisterMechanicState();
}

class _RegisterMechanicState extends State<RegisterMechanic> {
  var _chosenValue;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          appBar: AppBar(
            title: Center(
              child: Image.asset(
                "assets/images/ss-logo-white.png",
                height: 50,
              ),
            ),
            elevation: 0,
            actions: [
              IconButton(icon: Icon(Icons.help_outline), onPressed: () {})
            ],
            backgroundColor: Colours.red,
            shape: ContinuousRectangleBorder(
              borderRadius: const BorderRadius.only(
                bottomLeft: Radius.circular(20.0),
                bottomRight: Radius.circular(20.0),
              ),
            ),
          ),
          body: SingleChildScrollView(
              padding: EdgeInsets.all(20),
              child: Column(children: [
                Text(
                  'Enter the following details',
                  style: TextStyle(
                      color: Colours.red, fontWeight: FontWeight.bold),
                ),
                SizedBox(height: 20),
                TextFormField(
                  decoration: InputStyle('Full Name').main(),
                ),
                SizedBox(height: 10),
                InternationalPhoneNumberInput(
                  onInputChanged: (PhoneNumber number) {
                    print(number.phoneNumber);
                  },
                  onInputValidated: (bool value) {
                    print(value);
                  },
                  selectorConfig: SelectorConfig(
                    showFlags: false,
                    useEmoji: true,
                  ),
                  ignoreBlank: false,
                  autoValidateMode: AutovalidateMode.disabled,
                  selectorTextStyle: TextStyle(color: Colors.black),
                  formatInput: false,
                  keyboardType: TextInputType.numberWithOptions(
                      signed: true, decimal: true),
                  inputDecoration: InputDecoration(
                    filled: true,
                    fillColor: Colours.grey,
                    hintText: 'Mobile No.',
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0),
                      borderSide: BorderSide.none,
                    ),
                  ),
                  onSaved: (PhoneNumber number) {
                    print('On Saved: $number');
                  },
                ),
                SizedBox(height: 10),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colours.grey,
                  ),
                  child: DropdownButtonHideUnderline(
                    child: DropdownButton<String>(
                      isExpanded: true,
                      hint: Text('Photo ID Proof'),
                      value: _chosenValue,
                      items: [
                        DropdownMenuItem<String>(
                          child: Text('Aadhar Card'),
                          value: 'Aadhar Card',
                        ),
                        DropdownMenuItem<String>(
                          child: Text('Pan Card'),
                          value: 'Pan Card',
                        ),
                        DropdownMenuItem<String>(
                          child: Text('Voter ID Card'),
                          value: 'Voter ID Card',
                        ),
                      ],
                      onChanged: (String value) {
                        setState(() {
                          _chosenValue = value;
                        });
                      },
                    ),
                  ),
                ),
                SizedBox(height: 10),
                TextFormField(
                  decoration: InputStyle('Photo ID No.').main(),
                ),
                SizedBox(height: 10),
                Text(
                  'Upload a profile photo',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                SizedBox(height: 10),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Card(
                      child: ClipRRect(
                          borderRadius: BorderRadius.circular(5),
                          child: Image.asset(
                            'assets/images/placeholder_person.PNG',
                            height: 100,
                          )),
                    ),
                    TextButton(
                      onPressed: () {},
                      child:
                          Text('Upload', style: TextStyle(color: Colors.black)),
                      style: TextButton.styleFrom(
                        backgroundColor: Colours.grey,
                      ),
                    )
                  ],
                ),
                SizedBox(height: 20),
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pushNamedAndRemoveUntil(
                        dashb, (Route<dynamic> route) => false,
                        arguments: 'mechanic');
                  },
                  child: Text(
                    'CONTINUE',
                    style: TextStyle(color: Colors.white),
                  ),
                  style: TextButton.styleFrom(
                      backgroundColor: Colours.red,
                      minimumSize:
                          Size(MediaQuery.of(context).size.width * 0.9, 40),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20))),
                ),
              ]))),
    );
  }
}

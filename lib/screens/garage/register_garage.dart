import 'package:flutter/material.dart';
import 'package:star_services/utils/colours.dart';
import 'package:intl_phone_number_input/intl_phone_number_input.dart';
import 'package:star_services/utils/routes.dart';
import 'package:star_services/utils/inputstyle.dart';

class RegisterGarage extends StatefulWidget {
  const RegisterGarage({Key key}) : super(key: key);

  @override
  _RegisterGarageState createState() => _RegisterGarageState();
}

class _RegisterGarageState extends State<RegisterGarage> {
  var _chosenValue;
  bool _registered = false;
  @override
  Widget registered() {
    return Center(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image(
            width: double.infinity,
            image: AssetImage('assets/images/registered.PNG'),
          ),
          SizedBox(
            height: 20,
          ),
          Text(
            'You have successfully registered your garage',
            style: TextStyle(color: Colours.red, fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 30,
          ),
          TextButton(
              child: Text(
                'CONTINUE',
                style: TextStyle(color: Colors.white),
              ),
              style: TextButton.styleFrom(
                  backgroundColor: Colours.red,
                  minimumSize:
                      Size(MediaQuery.of(context).size.width * 0.9, 40),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20))),
              onPressed: () async {
                Navigator.of(context).pushNamedAndRemoveUntil(
                    dashb, (Route<dynamic> route) => false,
                    arguments: 'garage');
              }),
        ],
      ),
    );
  }

  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          appBar: AppBar(
            title: Center(
              child: Image.asset(
                "assets/images/ss-logo-white.png",
                height: 50,
              ),
            ),
            elevation: 0,
            actions: [
              IconButton(icon: Icon(Icons.help_outline), onPressed: () {})
            ],
            backgroundColor: Colours.red,
            shape: ContinuousRectangleBorder(
              borderRadius: const BorderRadius.only(
                bottomLeft: Radius.circular(20.0),
                bottomRight: Radius.circular(20.0),
              ),
            ),
          ),
          body: _registered
              ? registered()
              : SingleChildScrollView(
                  padding: EdgeInsets.all(20),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Center(
                          child: Text(
                            'Enter the following details',
                            style: TextStyle(
                                color: Colours.red,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        SizedBox(height: 20),
                        TextFormField(
                          decoration: InputStyle('Full Name').main(),
                        ),
                        SizedBox(height: 10),
                        TextFormField(
                          decoration: InputStyle('Garage Name').main(),
                        ),
                        Text('as registered in GST/PAN'),
                        SizedBox(height: 10),
                        TextFormField(
                          decoration: InputStyle('Address').main(),
                        ),
                        SizedBox(height: 10),
                        Row(
                          children: [
                            Expanded(
                              child: TextFormField(
                                decoration: InputStyle('City').main(),
                              ),
                            ),
                            SizedBox(width: 10),
                            Expanded(
                              child: TextFormField(
                                decoration: InputStyle('Pincode').main(),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 10),
                        TextFormField(
                          decoration: InputStyle('GST Number').main(),
                        ),
                        SizedBox(height: 10),
                        TextFormField(
                          decoration: InputStyle('Pan Number').main(),
                        ),
                        SizedBox(height: 30),
                        TextButton(
                          onPressed: () {
                            setState(() {
                              _registered = true;
                            });
                          },
                          child: Text(
                            'REGISTER',
                            style: TextStyle(color: Colors.white),
                          ),
                          style: TextButton.styleFrom(
                              backgroundColor: Colours.red,
                              minimumSize: Size(
                                  MediaQuery.of(context).size.width * 0.9, 40),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20))),
                        ),
                      ]))),
    );
  }
}

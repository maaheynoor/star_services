import 'package:flutter/material.dart';
import 'package:star_services/utils/colours.dart';
import 'package:star_services/utils/routes.dart';
import 'package:star_services/screens/select_services.dart';
import 'package:star_services/widgets/drawer.dart';
import 'package:star_services/screens/mechanic/mechanic_income.dart';
import 'package:star_services/screens/mechanic/mechanic_customer.dart';
import 'package:star_services/screens/mechanic/accept_customer.dart';
import 'package:star_services/screens/garage/select_services.dart';

class CustomerServices extends StatefulWidget {
  @override
  _CustomerServicesState createState() => _CustomerServicesState();
}

class _CustomerServicesState extends State<CustomerServices> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  int _selectedIndex = 0;
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  static List<Widget> _widgetOptions = <Widget>[
    AcceptedCustomer(),
    ServicesPage(),
    MechanicIncome(),
    ServicesPageGarage(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text('Periodic services'),
          elevation: 0,
          actions: [
            IconButton(icon: Icon(Icons.help_outline), onPressed: () {})
          ],
          backgroundColor: Colours.red,
          shape: ContinuousRectangleBorder(
            borderRadius: const BorderRadius.only(
              bottomLeft: Radius.circular(20.0),
              bottomRight: Radius.circular(20.0),
            ),
          ),
        ),
        drawer: ClipRRect(
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(10.0),
              bottomRight: Radius.circular(10.0)),
          child: Drawer(
            child: DrawerData(),
            semanticLabel: "drawer",
          ),
        ),
        body: SingleChildScrollView(
            padding: EdgeInsets.all(10),
            child: Column(children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Periodic Services',
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                  Text(
                    '15',
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                ],
              ),
              CustomerCard(service: 'Basic Service', customer: {
                'name': 'John Wick',
                'carmodel': 'Honda Accord',
                'address': '249 , S I H S Colony,Coimbatore, Mumbai- 400043'
              }),
              CustomerCard(service: 'Brake Maintenance', customer: {
                'name': 'Steve Rogers',
                'carmodel': 'Honda City',
                'address':
                    '249 , S I H S Colony, Aerodrome Post,Coimbatore, Mumbai- 400043'
              }),
            ])),
      ),
    );
  }
}

class CustomerCard extends StatelessWidget {
  CustomerCard({this.service, this.customer});
  final service, customer;
  Widget CustomText(title, value) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: Text.rich(TextSpan(text: title, children: [
        TextSpan(
          text: value,
          style: TextStyle(fontWeight: FontWeight.bold),
        )
      ])),
    );
  }

  Future<void> _showDialog(context) async {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text('Booking confirmed for ${customer["name"]}'),
              Text(
                'Booking ID - 12345',
                style: TextStyle(fontWeight: FontWeight.bold),
              )
            ],
          ),
          actions: <Widget>[
            SizedBox(
              width: MediaQuery.of(context).size.width,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      'OK',
                      style: TextStyle(color: Colors.white),
                    ),
                    style: TextButton.styleFrom(
                        backgroundColor: Colours.red,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20))),
                  ),
                ],
              ),
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.all(10),
        decoration: BoxDecoration(
            border: Border.all(width: 1, color: Colours.grey),
            borderRadius: BorderRadius.circular(5)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
                width: double.infinity,
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                    color: Colours.red,
                    borderRadius:
                        BorderRadius.vertical(top: Radius.circular(5))),
                child: Text(
                  service,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                )),
            CustomText('Customer Name - ', customer['name']),
            CustomText('Car Model - ', customer['carmodel']),
            CustomText('Address - ', customer['address']),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                CustomText('Date - ', '16/07/21'),
                CustomText('Time - ', '7:30 PM'),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Center(
              child: TextButton(
                onPressed: () {
                  _showDialog(context);
                },
                child: Text(
                  'CONFIRM BOOKING',
                  style: TextStyle(color: Colors.white),
                ),
                style: TextButton.styleFrom(
                    backgroundColor: Colours.red,
                    minimumSize:
                        Size(MediaQuery.of(context).size.width * 0.7, 40),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20))),
              ),
            ),
          ],
        ));
  }
}

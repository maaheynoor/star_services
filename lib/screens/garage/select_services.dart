import 'package:flutter/material.dart';
import 'package:star_services/utils/colours.dart';
import 'package:star_services/utils/routes.dart';
import 'package:star_services/widgets/squareCard.dart';
import 'package:star_services/screens/services_tab.dart';

class ServicesPageGarage extends StatelessWidget {
  List<String> services = [
    'Periodic Services',
    'Tyres & Wheel',
    'Car Spa',
    'Windshiels & Glass',
    'Custom Services',
    'Custom Services',
    'Windshiels & Glass',
    'Custom Services',
    'Custom Services',
  ];
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Padding(
          padding: const EdgeInsets.all(10),
          child: TextField(
            decoration: InputDecoration(
                contentPadding:
                    EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                filled: true,
                fillColor: Colours.grey,
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5.0),
                  borderSide: BorderSide.none,
                ),
                hintText: 'Search here',
                hintStyle: TextStyle(
                  color: Colors.black38,
                ),
                prefixIcon: Icon(Icons.search)),
          ),
        ),
        Image(
            image: AssetImage('assets/images/service.PNG'),
            width: double.infinity,
            fit: BoxFit.cover),
        SizedBox(width: 10),
        Padding(
          padding: const EdgeInsets.all(15.0),
          child: Text(
            'Select the services you want',
            style: TextStyle(color: Colours.red, fontWeight: FontWeight.bold),
          ),
        ),
        for (var i = 0; i < services.length / 3; i++)
          Card(
            child: ListTile(
              title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(services[i]),
                  CircleAvatar(
                    radius: 15,
                    backgroundColor: Colours.pink,
                    child: Text(
                      '03',
                      style: TextStyle(color: Colours.red),
                    ),
                  )
                ],
              ),
              onTap: () {
                Navigator.pushNamed(context, cust_services);
              },
              leading: CircleAvatar(
                radius: 25,
                backgroundImage: AssetImage('assets/images/user_icon.PNG'),
              ),
              trailing: Icon(Icons.arrow_forward_ios),
            ),
          ),
      ]),
    );
  }
}

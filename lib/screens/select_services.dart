import 'package:flutter/material.dart';
import 'package:star_services/utils/colours.dart';
import 'package:star_services/utils/routes.dart';
import 'package:star_services/widgets/squareCard.dart';
import 'package:star_services/screens/services_tab.dart';

class ServicesPage extends StatelessWidget {
  List<String> services = [
    'Periodic Services',
    'Tyres & Wheel',
    'Car Spa',
    'Windshiels & Glass',
    'Custom Services',
  ];
  @override
  Widget build(BuildContext context) {
    return Stack(children: <Widget>[
      SingleChildScrollView(
        child: Column(children: [
          Padding(
            padding: const EdgeInsets.all(10),
            child: TextField(
              decoration: InputDecoration(
                  contentPadding:
                      EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                  filled: true,
                  fillColor: Colours.grey,
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5.0),
                    borderSide: BorderSide.none,
                  ),
                  hintText: 'Search here',
                  hintStyle: TextStyle(
                    color: Colors.black38,
                  ),
                  prefixIcon: Icon(Icons.search)),
            ),
          ),
          Image(
              image: AssetImage('assets/images/service.PNG'),
              width: double.infinity,
              fit: BoxFit.cover),
          SizedBox(width: 10),
          Container(
            padding: EdgeInsets.all(10),
            color: Colours.grey,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.white),
                      child: Image.asset(
                        'assets/images/car_brands/honda.PNG',
                        height: 50.0,
                        width: 50.0,
                        fit: BoxFit.contain,
                      ),
                    ),
                    SizedBox(width: 10),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Honda WR-V',
                          style: TextStyle(
                              fontSize: 14, fontWeight: FontWeight.bold),
                        ),
                        Text('Petrol')
                      ],
                    ),
                  ],
                ),
                OutlinedButton(
                  onPressed: () {},
                  child: Text('Change'),
                  style: OutlinedButton.styleFrom(
                    primary: Colors.black,
                    side: BorderSide(color: Colors.black),
                  ),
                )
              ],
            ),
          ),
          for (var i = 0; i < services.length / 3; i++)
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                for (var j = 0; j < 3; j++)
                  (i * 3 + j < services.length)
                      ? SquareCard(
                          img: 'assets/images/service_icon.PNG',
                          title: services[i * 3 + j],
                          onTap: () {
                            Navigator.pushNamed(context, services_tab);
                          },
                          textStyle:
                              TextStyle(color: Colours.red, fontSize: 12))
                      : Container(
                          padding: EdgeInsets.all(10),
                          height: MediaQuery.of(context).size.width / 4,
                          width: MediaQuery.of(context).size.width / 4,
                        ),
              ],
            ),
          SizedBox(
            height: 50,
          )
        ]),
      ),
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: Align(
          alignment: Alignment.bottomRight,
          child: FloatingActionButton(
            onPressed: () {
              // Add your onPressed code here!
            },
            child: const Icon(Icons.call),
            backgroundColor: Colours.red,
          ),
        ),
      ),
    ]);
  }
}

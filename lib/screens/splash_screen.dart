import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:star_services/screens/onboarding.dart';
import 'package:star_services/utils/routes.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  SharedPreferences prefs;
  bool isOnboarding = true;
  @override
  initState() {
    super.initState();
    onNavigate();
  }

  void onNavigate() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool token = prefs.containsKey('token');
    isOnboarding = prefs.getBool("isonboarding") ?? true;
    if (token) {
      // Navigator.of(context).pushReplacementNamed(dashb);
    } else if (isOnboarding) {
      Navigator.of(context).pushReplacement(new PageRouteBuilder(
        maintainState: true,
        opaque: true,
        pageBuilder: (context, _, __) => new OnBoarding(),
      ));
    } else {
      Navigator.of(context).pushReplacementNamed(select_category);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

import 'package:flutter/material.dart';
import 'package:star_services/utils/colours.dart';
import 'package:star_services/utils/routes.dart';
import 'package:star_services/widgets/starRating.dart';

class SelectGarageScreen extends StatelessWidget {
  List garages = [
    {"name": "Star Services", "star": 4},
    {"name": "Brand New Auto", "star": 4},
    {"name": "Master Mechanic", "star": 3},
    {"name": "The Car Home", "star": 3}
  ];
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Periodic services'),
          elevation: 0,
          actions: [
            IconButton(icon: Icon(Icons.help_outline), onPressed: () {})
          ],
          backgroundColor: Colours.red,
          shape: ContinuousRectangleBorder(
            borderRadius: const BorderRadius.only(
              bottomLeft: Radius.circular(20.0),
              bottomRight: Radius.circular(20.0),
            ),
          ),
        ),
        body: SingleChildScrollView(
          child: Column(children: [
            Padding(
              padding: const EdgeInsets.all(10),
              child: TextField(
                decoration: InputDecoration(
                    contentPadding:
                        EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                    filled: true,
                    fillColor: Colours.grey,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0),
                      borderSide: BorderSide.none,
                    ),
                    hintText: 'Search here',
                    hintStyle: TextStyle(
                      color: Colors.black38,
                    ),
                    prefixIcon: Icon(Icons.search)),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Text(
                'Select a Garage',
                style:
                    TextStyle(color: Colours.red, fontWeight: FontWeight.bold),
              ),
            ),
            for (var i = 0; i < garages.length; i++)
              Card(
                child: ListTile(
                  title: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(garages[i]['name']),
                      StarRating(garages[i]['star']),
                    ],
                  ),
                  onTap: () {
                    Navigator.pushNamed(context, garage_detail);
                  },
                  leading: Image.asset(
                    'assets/images/garage.PNG',
                    width: 30,
                  ),
                  trailing: Icon(Icons.arrow_forward_ios),
                ),
              ),
          ]),
        ),
      ),
    );
  }
}

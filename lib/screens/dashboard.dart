import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:star_services/utils/colours.dart';
import 'package:star_services/utils/routes.dart';
import 'package:star_services/screens/select_services.dart';
import 'package:star_services/widgets/drawer.dart';
import 'package:star_services/screens/mechanic/mechanic_income.dart';
import 'package:star_services/screens/mechanic/mechanic_customer.dart';
import 'package:star_services/screens/mechanic/accept_customer.dart';
import 'package:star_services/screens/garage/select_services.dart';

class Dashboard extends StatefulWidget {
  Dashboard({this.user});
  final user;

  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  int _selectedIndex = 0;
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  static List<Widget> _widgetOptions = <Widget>[
    ServicesPage(),
    Container(),
    Container(),
    Container(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (widget.user == 'garage') {
      _widgetOptions = <Widget>[
        ServicesPageGarage(),
        Container(),
        MechanicIncome(),
        CustomerDetail(),
      ];
    } else if (widget.user == 'mechanic') {
      _widgetOptions = <Widget>[
        ServicesPageGarage(),
        Container(),
        MechanicIncome(),
        CustomerDetail(),
      ];
    } else {
      _widgetOptions = <Widget>[
        ServicesPage(),
        Container(),
        Container(),
        Container(),
      ];
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Center(
            child: Image.asset(
              "assets/images/ss-logo-white.png",
              height: 50,
            ),
          ),
          elevation: 0,
          actions: [
            IconButton(icon: Icon(Icons.help_outline), onPressed: () {})
          ],
          backgroundColor: Colours.red,
          shape: ContinuousRectangleBorder(
            borderRadius: const BorderRadius.only(
              bottomLeft: Radius.circular(20.0),
              bottomRight: Radius.circular(20.0),
            ),
          ),
          leading: IconButton(
            icon: Icon(FontAwesome5Solid.stream),
            onPressed: () => _scaffoldKey.currentState.openDrawer(),
          ),
        ),
        body: _widgetOptions.elementAt(_selectedIndex),
        drawer: ClipRRect(
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(10.0),
              bottomRight: Radius.circular(10.0)),
          child: Drawer(
            child: DrawerData(),
            semanticLabel: "drawer",
          ),
        ),
        bottomNavigationBar: ClipRRect(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20.0),
            topRight: Radius.circular(20.0),
          ),
          child: BottomNavigationBar(
            selectedFontSize: 0,
            items: const <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                icon: Icon(Icons.home_filled),
                backgroundColor: Colours.red,
                label: '',
              ),
              BottomNavigationBarItem(
                icon: Icon(
                  Icons.list_outlined,
                ),
                backgroundColor: Colours.red,
                label: '',
              ),
              BottomNavigationBarItem(
                icon: Icon(FontAwesome.line_chart),
                backgroundColor: Colours.red,
                label: '',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.person),
                backgroundColor: Colours.red,
                label: '',
              ),
            ],
            currentIndex: _selectedIndex,
            onTap: _onItemTapped,
            backgroundColor: Colours.red,
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:star_services/screens/dashboard.dart';
import 'package:star_services/utils/colours.dart';
import 'package:star_services/utils/routes.dart';
import 'package:star_services/screens/dashboard.dart';
import 'package:star_services/screens/select_services.dart';

class CarModel extends StatefulWidget {
  CarModel({this.brand});
  final String brand;

  @override
  _CarModelState createState() => _CarModelState();
}

class _CarModelState extends State<CarModel> {
  List<String> cars = ["City", "Civic", "Accord", "Jazz"];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            appBar: AppBar(
              title: Center(
                child: Image.asset(
                  "assets/images/ss-logo-white.png",
                  height: 50,
                ),
              ),
              elevation: 0,
              actions: [
                IconButton(icon: Icon(Icons.help_outline), onPressed: () {})
              ],
              backgroundColor: Colours.red,
              shape: ContinuousRectangleBorder(
                borderRadius: const BorderRadius.only(
                  bottomLeft: Radius.circular(20.0),
                  bottomRight: Radius.circular(20.0),
                ),
              ),
            ),
            body: SingleChildScrollView(
              padding: EdgeInsets.all(10),
              child: Column(children: [
                TextField(
                  decoration: InputDecoration(
                      contentPadding:
                          EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                      filled: true,
                      fillColor: Colours.grey,
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5.0),
                        borderSide: BorderSide.none,
                      ),
                      hintText: 'Search by modal or brand name',
                      hintStyle: TextStyle(
                        color: Colors.black38,
                      ),
                      prefixIcon: Icon(Icons.search)),
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Text(
                    'Select Your Car Model',
                    style: TextStyle(
                        color: Colours.red, fontWeight: FontWeight.bold),
                  ),
                ),
                Text(widget.brand + ' Cars'),
                for (var i = 0; i < cars.length; i++) CarCard(car: cars[i])
              ]),
            )));
  }
}

class CarCard extends StatefulWidget {
  CarCard({this.car});
  final car;

  @override
  _CarCardState createState() => _CarCardState();
}

class _CarCardState extends State<CarCard> {
  var _chosenValue = "Petrol";
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        GestureDetector(
          onTap: () {
            Navigator.of(context).pushNamedAndRemoveUntil(
                dashb, (Route<dynamic> route) => false,
                arguments: 'user');
          },
          child: Row(
            children: [
              SizedBox(
                width: 50,
              ),
              Expanded(
                child: Card(
                  child: ListTile(
                    title: Text(widget.car),
                    leading: SizedBox(
                      width: 50,
                    ),
                    trailing: Container(
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      height: 30,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(30),
                        color: Colours.red,
                      ),
                      child: DropdownButton<String>(
                        value: _chosenValue,
                        style: TextStyle(color: Colors.white),
                        dropdownColor: Colours.red,
                        items: <String>['Petrol', 'Diesel', 'CNG']
                            .map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                        onChanged: (String value) {
                          setState(() {
                            _chosenValue = value;
                          });
                        },
                        icon: Icon(Icons.arrow_drop_down_circle_outlined,
                            color: Colors.white),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        Image(
          image: AssetImage('assets/images/car_brands/car.PNG'),
          height: 50,
        ),
      ],
    );
  }
}

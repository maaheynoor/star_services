import 'package:flutter/material.dart';
import 'package:star_services/screens/select_category.dart';
import 'package:star_services/screens/register.dart';
import 'package:star_services/screens/otp_screen.dart';
import 'package:star_services/screens/car_brand.dart';
import 'package:star_services/screens/car_model.dart';
import 'package:star_services/screens/dashboard.dart';
import 'package:star_services/screens/select_garage.dart';
import 'package:star_services/screens/garage_detail.dart';
import 'package:star_services/screens/call_screen.dart';
import 'package:star_services/screens/mechanic/register_mechanic.dart';
import 'package:star_services/screens/mechanic/accept_customer.dart';
import 'package:star_services/screens/garage/register_garage.dart';
import 'package:star_services/screens/garage/services.dart';
import 'package:star_services/screens/services_tab.dart';

const String select_category = '/select_category';
const String register = '/register';
const String register_mechanic = '/register_mechanic';
const String register_garage = '/register_garage';
const String otp = '/otp';
const String car_brand = '/car_brand';
const String car_model = '/car_model';
const String dashb = '/dashb';
const String select_garage = '/select_garage';
const String garage_detail = '/garage_detail';
const String call_screen = '/call_screen';
const String accept_customer = '/accept_customer';
const String cust_services = '/cust_services';
const String services_tab = '/services_tab';

class CustomRouter {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case select_category:
        return MaterialPageRoute(
            settings: settings, builder: (_) => CategoryPage());
      case register:
        return MaterialPageRoute(
            settings: settings, builder: (_) => Register());
      case register_mechanic:
        return MaterialPageRoute(
            settings: settings, builder: (_) => RegisterMechanic());
      case register_garage:
        return MaterialPageRoute(
            settings: settings, builder: (_) => RegisterGarage());
      case otp:
        return MaterialPageRoute(settings: settings, builder: (_) => OTP());
      case car_brand:
        return MaterialPageRoute(
            settings: settings, builder: (_) => CarBrand());
      case car_model:
        return MaterialPageRoute(
            settings: settings,
            builder: (_) => CarModel(brand: settings.arguments));
      case dashb:
        return MaterialPageRoute(
            settings: settings,
            builder: (_) => Dashboard(user: settings.arguments));
      case select_garage:
        return MaterialPageRoute(
            settings: settings, builder: (_) => SelectGarageScreen());
      case garage_detail:
        return MaterialPageRoute(
            settings: settings, builder: (_) => GarageDetail());
      case call_screen:
        return MaterialPageRoute(
            settings: settings,
            builder: (_) => CallScreen(
                  mechanic: settings.arguments,
                ));
      case accept_customer:
        return MaterialPageRoute(
            settings: settings, builder: (_) => AcceptedCustomer());
      case cust_services:
        return MaterialPageRoute(
            settings: settings, builder: (_) => CustomerServices());
      case services_tab:
        return MaterialPageRoute(
            settings: settings, builder: (_) => ServicesTabsScreen());
    }
  }
}

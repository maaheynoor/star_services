import 'package:flutter/material.dart';
import 'package:star_services/utils/colours.dart';

class InputStyle {
  String label;
  int errLines;
  InputStyle(label, {errLines}) {
    this.label = label;
    this.errLines = errLines;
  }
  main() {
    print(errLines);
    return InputDecoration(
      contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      filled: true,
      fillColor: Colours.grey,
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(5.0),
        borderSide: BorderSide.none,
      ),
      hintText: label,
      hintStyle: TextStyle(
        color: Colors.black38,
      ),
      errorStyle: TextStyle(
        color: Colours.red,
      ),
      errorMaxLines: errLines ?? 1,
    );
  }

  datepicker() {
    return InputDecoration(
        contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
        filled: true,
        fillColor: Colours.grey,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5.0),
          borderSide: BorderSide.none,
        ),
        hintText: label,
        hintStyle: TextStyle(
          color: Colors.black38,
          fontSize: 14,
        ),
        errorStyle: TextStyle(
          color: Colours.red,
        ),
        suffixIcon: IconButton(
          icon: Icon(Icons.calendar_today_outlined),
          color: Colors.black54,
          onPressed: () {},
        ));
  }

  search() {
    return InputDecoration(
      contentPadding: EdgeInsets.all(5),
      filled: true,
      fillColor: Colors.white,
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(5.0),
        borderSide: BorderSide.none,
      ),
      hintText: label,
      hintStyle: TextStyle(
        color: Colors.black38,
        fontSize: 14,
      ),
      errorStyle: TextStyle(
        color: Colours.red,
      ),
      prefixIcon: Icon(
        Icons.search,
        color: Colors.black38,
      ),
    );
  }
}

import 'package:flutter/material.dart';

class Colours {
  static const red = Color(0xffff3044);
  static const pink = Color(0xffffeaec);
  static const grey = Color(0xffeaeaea);
}
